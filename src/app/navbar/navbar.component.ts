import { Component } from '@angular/core';

/**
 * This class represents the navigation bar component.
 */
@Component({
  selector: 'sbs-navbar',
  templateUrl: 'navbar.component.html',
  styleUrls: ['navbar.component.scss']
})
export class NavbarComponent {}
