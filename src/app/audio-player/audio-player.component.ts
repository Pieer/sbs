import { Component , OnInit, OnDestroy, AfterViewInit, Input} from '@angular/core';
import { MediaService } from '../media-controller/media-controller.service';
import { OverlayService } from '../overlay/overlay.service';

declare var WebAPI: any, window: any;
interface Window { AudioContext: any; }
window.AudioContext = window.AudioContext || window.webkitAudioContext || window.msAudioContext || window.mozAudioContext;

/**
 * This class represents the lazy loaded AudioComponent.
 */
@Component({
  selector: 'sbs-audio-player',
  templateUrl: 'audio-player.component.html',
  styleUrls: [ 'audio-player.component.scss' ]
})
export class AudioPlayerComponent implements AfterViewInit, OnInit, OnDestroy {
  @Input() colour:string;
  @Input() src:string;

  arr:Array<number>;
  num:number;
  bars:any;
  audio:any;
  visualWidth:any;
  visualHeight:any;
  audioContext:any;
  source:any;
  analyser:any;
  bufferLength:any;
  frequencyData:any;
  scaleBarSum:any;
  darkerColor:any;

  constructor(private overlayService: OverlayService, private mediaService: MediaService) {
    this.num = 20;
    this.arr = Array(this.num).map((x:any,i:any)=>i);

    overlayService.close.subscribe( () => {
      this.audio.pause();
    });

    mediaService.play.subscribe( () => {
      this.play();
    });

    mediaService.pause.subscribe( () => {
      this.pause();
    });

    mediaService.restart.subscribe( () => {
      this.restart();
    });
  }

  ngOnInit() {
    this.darkerColor = this.colorLuminance(this.colour,-0.3);
  }

  ngAfterViewInit() {
    this.bars = document.querySelectorAll('.player-spectrum-bar');
    this.audio = document.querySelector('audio');
    this.visualWidth = window.innerWidth;
    this.visualHeight = window.innerHeight/2;
    this.audioContext = new AudioContext();
    this.audio.crossOrigin = "anonymous";
    this.source = this.audioContext.createMediaElementSource(this.audio);
    this.analyser = this.audioContext.createAnalyser();
    this.audio.oncanplay = () => {
      this.analyse();
    };
    this.audio.src = this.src;
    this.audio.play();
  }

  ngOnDestroy() {
    this.audio.pause();
  }

  play() {
    if(this.audio.paused) {
      this.audio.play();
      this.visualStart();
    }
  }

  pause() {
    this.audio.pause();
  }

  restart() {
    this.audio.currentTime = 0;
  }

  analyse() {
    this.source.connect(this.analyser);
    this.analyser.connect(this.audioContext.destination);
    this.analyser.fftSize = 2048;
    this.analyser.minDecibels = -90;
    this.analyser.maxDecibels = 0;

    this.bufferLength = this.analyser.frequencyBinCount;
    this.frequencyData = new Uint8Array(this.bufferLength);

    this.visualStart();
  }

  colorLuminance(hex:string, lum:number) {

    // validate hex string
    hex = String(hex).replace(/[^0-9a-f]/gi, '');
    if (hex.length < 6) {
      hex = hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2];
    }
    lum = lum || 0;

    // convert to decimal and change luminosity
    let rgb:string = '#', c:any, i:number;
    for (i = 0; i < 3; i++) {
      c = parseInt(hex.substr(i*2,2), 16);
      c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
      rgb += ('00'+c).substr(c.length);
    }

    return rgb;
  }

  scaleBar(fromMin:number, fromMax:number, valueIn:any):number {
    let toMin = 0,
        toMax = this.visualHeight;
    //fromMin = fromMax * .4;
    let result = ((toMax - toMin) * (valueIn - fromMin)) / (fromMax - fromMin) + toMin;
    if(isNaN(result)) result = 0;

    return Math.abs(toMax - result);
  };

  visualStart() {
    this.analyser.getByteFrequencyData(this.frequencyData);

    //var frequencyWidth = (this.visualWidth / this.bufferLength) * 2,
    let frequencyHeight = 0, scales:Array<number> = [], fd:Array<number> = [], bars = 0,
        fdMin = Math.min.apply(Math,this.frequencyData),
        fdMax = Math.max.apply(Math,this.frequencyData);

    for (var increment = 0; increment < this.bufferLength; increment+=32) {
      frequencyHeight = this.scaleBar(fdMin, fdMax, this.frequencyData[increment]);
      var y = this.visualHeight - frequencyHeight;

      if (increment % 16 !== 0 && increment > 32) {
        return;
      } else {
        bars++;
      }

      if (increment < 16) {
        scales.push(frequencyHeight / 32);
      }

      fd.push(this.frequencyData[increment]);

      if (bars > 32) {
        bars = 1;
      }

      if(this.bars[bars-1] !== undefined) {
        this.bars[bars-1].setAttribute('style','background-color:'+this.colour+';height:'+ y + 'px;');
      }
    }

    var sc = scales.reduce(function(pv, cv) { return pv + cv; }, 0) / scales.length;
    this.scaleBarSum = fd.reduce(function(pv, cv) { return pv + cv; }, 0) / fd.length;
    sc *= 0.25;
    // //document.querySelector('.player-cover').style.transform = 'scale('+ sc +')';
    //this.MusicVisuals.call = requestAnimationFrame(MusicVisuals.start);

    if(!this.audio.paused) {
      setTimeout( () =>{
        this.visualStart()
      }, 30);
      // requestAnimationFrame(this.visualStart.bind(this));
    }

  }
}
