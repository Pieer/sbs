import { Component, Input, OnChanges, HostBinding } from '@angular/core';
import { Story } from '../story/story';
import { MediaService } from '../media-controller/media-controller.service';
import { YoutubeService } from '../video-player/video-player.service';

/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({

  selector: 'sbs-story-detail',
  templateUrl: 'story-detail.component.html',
  styleUrls: [ 'story-detail.component.scss' ],
  providers:  [ MediaService ]
})
export class StoryDetailComponent implements OnChanges {
  @Input() title:string;
  @Input() colour:string;
  @Input() story: Story;
  @HostBinding('class.show-play') showPlay:boolean;

  private desktop:boolean;

  constructor(private youtubeService: YoutubeService) {
    this.desktop = window.innerWidth > 768;
  }

  ngOnChanges() {
    if(this.story.video) {
      this.youtubeService.launchPlayer(this.story.video, 'video-placeholder-story-'+this.story.id, true);
    }
  }

  private playVideo() {
    this.showPlay = true;
    this.youtubeService.play()
  }

  private stopVideo() {
    this.showPlay = false;
    this.youtubeService.pause()
  }
}
