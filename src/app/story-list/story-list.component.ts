import { Component, Input  } from '@angular/core';
import { Story } from '../story/story';
import { Chapter } from './../chapter/chapter';
import { OverlayService } from '../overlay/overlay.service';

/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({

  selector: 'sbs-story-list',
  templateUrl: 'story-list.component.html',
  styleUrls: [ 'story-list.component.scss' ]
})
export class StoryListComponent {
  @Input() chapters: any;

  chapter: Chapter;
  storyId: number;
  storyNum: string;
  storyNumOld: string;
  story: Story;
  stories: any;

  constructor(private overlayService: OverlayService) {
    overlayService.select.subscribe( ( selected:any ) => {
      this.chapter = this.chapters[selected.chapterId];
      this.storyId = selected.storyId;
      this.calcNum();
      this.story = this.chapter.stories[this.storyId];
      this.stories = [];
      for (let story_id in this.chapter.stories) {
        let story = this.chapter.stories[story_id];

        if(story.type !== 'text') {
          this.stories.push(story);
        }

        if(story_id == selected.storyId) {
          var s = String(this.stories.length);
          this.storyNum = this.stories.length < 10 ? '0'+s : s;
          this.storyId = parseInt(this.stories.length)-1;
        }
      }
    });

    overlayService.close.subscribe( () => {
      setTimeout( () => {
        this.story = null;
      }, 500);
    });

    overlayService.down.subscribe( () => {
      this.nextChapter();
    });

    overlayService.up.subscribe( () => {
      this.prevChapter();
    });
  }

  calcNum() {
    // if(this.storyNum)
    this.storyNumOld = this.storyNum;
    let num = this.storyId+1;
    this.storyNum = num < 10 ? '0'+String(num) : String(num);
  }

  prevChapter() {
    if(this.storyId > 0) {
      this.storyId = this.storyId - 1;
      this.calcNum();
      this.overlayService.switch();
      setTimeout( () => {
        this.story = this.stories[this.storyId];
      }, 1000);
    }
  }

  nextChapter() {
    if(this.storyId < this.stories.length -1) {
      this.storyId = this.storyId + 1;
      this.calcNum();
      this.overlayService.switch();
      setTimeout( () => {
        this.story = this.stories[this.storyId];
      }, 1000);
    }
  }
}
