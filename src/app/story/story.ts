export class Story {
    id: number;
    title: string;
    content: string;
    type: string;
    video: string;
    source: string;
}
