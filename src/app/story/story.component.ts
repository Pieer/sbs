import { Component, Input } from '@angular/core';
import { Story } from './story';
import { OverlayService } from '../overlay/overlay.service';

/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
  selector: 'sbs-story',
  templateUrl: 'story.component.html',
  styleUrls: [ 'story.component.scss' ]
})
export class StoryComponent {
  @Input() story:Story;
  @Input() index:number;
  @Input() colour:string;
  @Input() chapterIndex: number;

  constructor(private overlayService: OverlayService) {}

  showDetail() {
    if(this.story.type !== 'text') {
      this.overlayService.selectChapter(this.index, this.chapterIndex);
    }
  }
}
