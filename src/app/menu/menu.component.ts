import { Component , Input, HostBinding, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Chapter } from '../chapter/chapter';
import { OverlayService } from '../overlay/overlay.service';
import { ScrollService } from '../scroll.service';

@Component({
  selector: 'sbs-menu',
  templateUrl: 'menu.component.html',
  styleUrls: [ 'menu.component.scss' ]
})
export class MenuComponent implements OnInit {
  @Input() chapters:Array<Chapter> = [];
  @HostBinding('class.show-menu') showMenu:boolean;
  @HostBinding('class.fade-in') fadeIn:boolean;
  navChapters:Array<Chapter> = [];
  background:any;
  backgroundOld:string;
  activeBackground:boolean;
  activeBackgroundOld:boolean;
  activeBackgroundDark:boolean;
  showTimeOut: any;

  constructor(private overlayService: OverlayService, private sanitizer:DomSanitizer, private scrollService: ScrollService) {
    this.fadeIn = false;


    overlayService.open.subscribe( () => {
      this.scrollService.speed = 800;
      this.showMenu = true;
      this.showTimeOut = setTimeout( () => {
        this.fadeIn = true;
      }, 1000);
    });

    overlayService.close.subscribe( () => {
      clearTimeout(this.showTimeOut);
      this.fadeIn = false;
      this.showTimeOut = setTimeout( () => {
        this.showMenu = false;
      }, 500);
    });
  }

  ngOnInit():void {
    this.navChapters = this.chapters.slice(1,this.chapters.length);
  }

  public closeNav(event:any) {
    event.stopPropagation();
    clearTimeout(this.showTimeOut);
    this.fadeIn = false;
    this.showTimeOut = setTimeout( () => {
      this.showMenu = false;
      this.overlayService.closeOverlay();
    }, 1000);
  }

  public showBackground(chapterId:number) {
    this.background = this.sanitizer.bypassSecurityTrustStyle('url(' + this.chapters[chapterId+1].background + ')');
    this.activeBackgroundOld = false;
    this.activeBackground = true;
    this.activeBackgroundDark = true;
    setTimeout(() => {
      if(this.background !== 'none') {
        this.backgroundOld = this.background;
      }
      if(this.activeBackgroundDark) {
        this.background = 'none';
        this.activeBackground = false;
        this.activeBackgroundOld = true;
      }
    }, 800);
  }

  public hideBackground() {
    this.activeBackgroundOld = false;
    this.activeBackground = false;
    this.activeBackgroundDark = false;
  }
}
