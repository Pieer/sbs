import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';
declare var ScrollMagic: any;

@Injectable()
export class ScrollService {

  public currentChapter: number = -1;
  public isOverlay:boolean = false;
  public goingDown:boolean = true;
  public direction: string;
  public nextElement: string;
  public previousChapter: number;
  public speed: number = 0;
  public oldTimeStamp: number = 0;

  public skipTo:number = 0;

  // Observable string sources
  public transitionEndSource = new Subject<string>();
  public transitionMaskEndSource = new Subject<string>();

  // Observable string streams
  public transitionEnd = this.transitionEndSource.asObservable();
  public transitionMaskEnd = this.transitionMaskEndSource.asObservable();

  private controller: any;

  constructor() {
    this.controller = new ScrollMagic.Controller({container: "body"});
  }

  // Service message commands
  public goNext(direction: string, element: string) {
    this.nextElement = element;
    this.direction = direction;
    this.isOverlay = element === 'overlay';
    this.goingDown = direction !== 'REVERSE';

    let el:any;
    this.previousChapter = this.currentChapter;

    if(this.goingDown) {
      if(!this.isOverlay) {
        this.currentChapter++;
      }
      el = this.nextElement === 'chapter' ? this.currentChapter : 'overlay-'+ this.currentChapter;
    } else {
      // If going up change chapter if next element is overlay
      if(this.isOverlay) {
        this.goingDown ? this.currentChapter++ : this.currentChapter--;
      }
      el = this.nextElement === 'chapter' ? 'overlay-'+ this.currentChapter : this.currentChapter + 1;
    }

    this.transitionEndSource.next(el);
  }

  public endTransition(id: string) {
    this.transitionMaskEndSource.next(id);
  }

  public updateSpeed(e:any) {
    this.speed = e.timeStamp - this.oldTimeStamp;
    this.oldTimeStamp = e.timeStamp;
  }

  public addScene(el:any, handler:any, offset:any) {
    let self = this;

    new ScrollMagic.Scene({
      triggerElement: el,
      offset: offset,
      duration: 0
    })
    .on('start end', (args) => {
      handler(args);
      if(window.innerWidth > 768) {
        self.updateSpeed(args);
      } else {
        this.speed = 2000;
      }
    })
    .addTo(self.controller);

  }

  public addPanel(id:string) {
    let self = this;

    new ScrollMagic.Scene({
      triggerElement: id,
      offset: -200,
    })
        .setClassToggle(id, 'active')
        .addTo(self.controller);
  }

  public skip(id:number) {
    this.skipTo = id;
    setTimeout( () => {
      this.skipTo = 0;
    },500);
  }
}
