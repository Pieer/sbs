import { Component, HostBinding } from '@angular/core';
import { ScrollService } from '../scroll.service';

/**
 * This class represents the lazy loaded FooterComponent.
 */
@Component({
  selector: 'sbs-footer',
  templateUrl: 'footer.component.html',
  styleUrls: [ 'footer.component.scss']
})
export class FooterComponent {
  @HostBinding('class.active') active:boolean;

  constructor(private scrollService: ScrollService) {
    scrollService.transitionEnd.subscribe(() => {
      if (!scrollService.goingDown) {
        if(scrollService.currentChapter  === 6) {
          this.active = false;
        }
      }
    });
    scrollService.transitionMaskEnd.subscribe(() => {
      if (scrollService.goingDown) {
        if(scrollService.currentChapter  === 7) {
          this.active = true;
        }
      }
    });
  }

  public goToTop() {
    window.scrollTo(0,0);
  }
}
