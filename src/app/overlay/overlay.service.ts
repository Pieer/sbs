import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';

@Injectable()
export class OverlayService {

  public currentChapter: number = -1;
  public isFrozen:boolean = false;

  // Observable string sources
  public freezeSource = new Subject<boolean>();
  public selectChapterSource = new Subject<any>();
  public openSource = new Subject<string>();
  public closeOverlaySource = new Subject<string>();
  public fullScreenSource = new Subject<string>();
  public upSource = new Subject<string>();
  public downSource = new Subject<string>();
  public switchSource = new Subject<string>();

  // Observable string streams
  public freeze = this.freezeSource.asObservable();
  public select = this.selectChapterSource.asObservable();
  public open = this.openSource.asObservable();
  public close = this.closeOverlaySource.asObservable();
  public fullScreen = this.fullScreenSource.asObservable();
  public up = this.upSource.asObservable();
  public down = this.downSource.asObservable();
  public switchContent = this.switchSource.asObservable();

  selectChapter(storyId: number, chapterId: number) {
    this.toggleScroll(true);
    let selection = {
      storyId: storyId,
      chapterId: chapterId
    };
    this.selectChapterSource.next(selection);
  }

  toggleScroll(freeze: boolean) {
    this.isFrozen = freeze;
    this.freezeSource.next(freeze);
  }

  closeOverlay() {
    this.closeOverlaySource.next('');
  }

  openNav() {
    this.openSource.next('');
  }

  goFullScreen() {
    this.fullScreenSource.next('');
  }

  prev() {
    this.upSource.next('');
  }

  next() {
    this.downSource.next('');
  }

  switch() {
    this.switchSource.next('');
  }
}
