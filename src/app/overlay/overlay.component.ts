import { Component, HostBinding } from '@angular/core';
import { OverlayService } from '../overlay/overlay.service';

/**
 * This class represents the lazy loaded OverlayComponent.
 */
@Component({
  selector: 'sbs-overlay',
  templateUrl: 'overlay.component.html',
  styleUrls: [ 'overlay.component.scss' ]
})
export class OverlayComponent {

  @HostBinding('class.show-list') showList:boolean;
  @HostBinding('class.show-menu') showMenu:boolean;
  @HostBinding('class.active') activeOverlay:boolean;
  @HostBinding('class.opened') opened:boolean;
  @HostBinding('class.switch') switch:boolean;

  private body: any;
  private fullscreen: boolean = false;


  constructor(private overlayService: OverlayService) {
    this.body = document.querySelector('body');
    this.switch = false;

    overlayService.select.subscribe( () => {
      this.showList = true;
      this.open(10);
    });

    overlayService.open.subscribe( () => {
      this.showMenu = true;
      this.body.className = 'freeze';
      this.activeOverlay = true;
      setTimeout( () => {
        this.opened = true;
      }, 10);
    });

    overlayService.close.subscribe( () => {
      this.opened = false;
      setTimeout( () => {
        this.close();
      }, 1000);
    });

    overlayService.fullScreen.subscribe( () => {
      this.fullscreen = !this.fullscreen;
    });

    overlayService.switchContent.subscribe( () => {
      this.switch = true;
      setTimeout( () => {
        this.switch = false;
      }, 1000);
    });
  }

  public open(time: number) {
    this.body.className = 'freeze';
    this.activeOverlay = true;
    setTimeout( () => {
      this.opened = true;
    },time);
  }

  closeOverlay() {
    this.overlayService.closeOverlay();
  }

  public close() {
    this.fullscreen = false;
    this.body.className = '';
    this.activeOverlay = false;
    this.showMenu = false;
    this.showList = false;
  }

}

