import {Directive, OnInit, Input, ElementRef, NgZone} from '@angular/core';
import { ScrollService } from '../scroll.service';
import { OverlayService } from '../overlay/overlay.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/filter';

/**
 * This class represents the svg-mask component.
 */
@Directive({
  selector: '[wave]'
})

export class WaveMaskDirective implements OnInit {
  @Input() id:any;
  @Input() mask:any;

  // Optionals inputs
  @Input() speedTransition:number = 35;
  @Input() relativeAmplitude:number = 2;
  @Input() frequency:number = 15;

  private direction:string;
  private width:number;
  private height:number;
  private maskHeight:number;
  private amplitude:number;
  private counter:number;
  private el: HTMLElement;
  private currentChapter: string;

  constructor(el: ElementRef, private scrollService: ScrollService, private overlayService: OverlayService, private zone: NgZone) {
    this.width = window.innerWidth;
    this.height = window.innerHeight;
    this.amplitude = this.height/this.relativeAmplitude;
    this.maskHeight = this.height;
    this.el = el.nativeElement;
    this.el.setAttribute('points', this.createWave(.001,this.width,0));
    this.counter = Math.floor(Math.random()*100);

    const $resizeEvent =
        Observable.fromEvent(window, 'resize', ()=> { return { width: document.documentElement.clientWidth, height: document.documentElement.clientHeight }})
            .debounceTime(200)

    setTimeout( () => {
      $resizeEvent.subscribe(data => {
        this.resizeAll(data);
      });
    }, 2000)

    scrollService.transitionEnd.subscribe(
        (el) => {
          this.direction = scrollService.direction;
          this.currentChapter = el;
          setTimeout(() => {
            if(el === this.id) {
              let cc = scrollService.currentChapter;
              let ci = parseInt(String(this.id).replace ( /[^\d.]/g, '' ));
              if(this.direction === 'REVERSE' && scrollService.isOverlay) {
                cc++;
              }
              if(ci !== cc) {
                this.skipTransition();
              } else {
                this.draw();
              }
            }
          },10);
        });
  }

  ngOnInit() {
    if(this.id === 'nav') {
      this.overlayService.open.subscribe(() => {
        this.direction = 'FORWARD';
        this.draw();
      });

      this.overlayService.close.subscribe(() => {
        this.direction = 'REVERSE';
        this.draw();
      });
    }
  }

  /**
   * Define wave points coordinates
   *
   * @param {pointNumber} Fraction to get number of points on the line
   * @param {length} Length of the wave
   * @param {cycle} Position of the wave
   */
  private createWave(pointNumber:number,length:number,cycle:number) {
    let points = ['0',(this.amplitude*2)];
    let width = length;
    let x = 0, y:number;
    while (x++ <= width) {
      y = Math.sin(x*pointNumber+cycle);
      points.push([(x),this.maskHeight + (y*this.amplitude/2 + this.amplitude/2)].join(' '));
    }

    points.push([length,this.amplitude*2].join(' '));
    points.push([0,this.amplitude*2].join(' '));

    return points.toString();
  }

  /**
   * Draw the mask according to direction of the wave
   */
  private draw() {
    let speed = this.speedTransition / 2;
    if(this.scrollService.speed < 1000) {
      speed = this.speedTransition;
      if(this.scrollService.speed < 500) {
        speed = this.speedTransition * 2;
      } else {
        speed = this.speedTransition * 4;
      }
    }

    // Avoid intro quick transition
    if(parseInt(String(this.id).replace ( /[^\d.]/g, '' )) === 1) {
      this.speedTransition = 30
    }

    this.zone.runOutsideAngular(() => {
      // Update period
      let cycle = ++this.counter/this.frequency;
      this.el.setAttribute('points', this.createWave(.001,this.width,cycle));
      // Move mask up or down according to direction
      this.maskHeight += this.direction !== 'REVERSE' ? -speed : speed;
      // Check if the transition is completed
      const atTop    = this.maskHeight <= -(this.amplitude+speed);
      const atBottom = this.maskHeight >= this.height + this.amplitude + speed;
      const complete = this.direction !== 'REVERSE' ? atTop : atBottom;
      // Animated mask up or down id not at the end
      if(complete) {
        requestAnimationFrame(() => {
          this.scrollService.endTransition(this.id);
        });
      } else {
        requestAnimationFrame(this.draw.bind(this));
      }

    });
  }

  private skipTransition() {
    this.scrollService.endTransition(this.id);
    setTimeout(() => {
      this.maskHeight = this.direction !== 'REVERSE' ? -(this.amplitude+this.speedTransition) : this.height;
      this.draw();
    }, 1000);
  }

  private resizeAll (data) {
    if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      location.reload();
    }
    // if(parseInt(this.currentChapter)-1 === parseInt(this.id)-1 || this.currentChapter === this.id) {
    //   console.log(this.id, data)
    //   this.width = data.width;
    //   this.height = data.height;
    //   this.amplitude = this.height/this.relativeAmplitude;
    //   this.maskHeight = this.height;
    //   this.draw();
    // }
  }
}
