import { Component, Input } from '@angular/core';

/**
 * This class represents the svg-overlay component.
 */
@Component({
    selector: 'svg-overlay',
    template: `
    <svg class="sbs-svg-mask-wrapper">
      <defs>
        <clipPath id="mask-{{id}}">
          <polygon wave [id]="id"></polygon>
        </clipPath>
      </defs>
      
      <g attr.clip-path="url(#mask-{{id}})">
        <rect width="100%" height="100%" [style.fill]="colour" class="sbs-chapter-overlay"></rect>
      </g>
    </svg>
  `,
    styleUrls: [ 'svg-mask.component.scss' ]
})

export class SvgOverlayComponent {
    @Input() colour:any;
    @Input() id: any;
}
