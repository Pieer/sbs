import { Component, Input , OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ScrollService } from '../scroll.service';


/**
 * This class represents the svg-mask component.
 */
@Component({

  selector: 'svg-mask',
  templateUrl: 'svg-mask.component.html',
  styleUrls: [ 'svg-mask.component.scss' ]
})

export class SvgMaskComponent implements OnInit {
  @Input() chapter:any;
  @Input() isOverlay:boolean;

  public backgroundTextStyle: any;
  public placeholder: string;
  public maskId: any;
  public subtitle_line1:string;
  public subtitle_line2:string;
  public display: any;
  private desktop:boolean;
  private titlePosition:string;


  constructor(private sanitizer: DomSanitizer, private scrollService: ScrollService) {
    this.desktop = window.innerWidth > 1200;

    this.titlePosition = 'translate(0,'+(this.desktop?'33':'18')+')';

    // Fix safari bug...
    if (navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1)  {
      scrollService.transitionMaskEnd.subscribe( () => {
        this.display = this.sanitizer.bypassSecurityTrustStyle('inline-block');
        setTimeout(() => {
          this.display = this.sanitizer.bypassSecurityTrustStyle('block');
        }, 100);
      });
    }
  }

  ngOnInit() {
    this.placeholder = 'video-placeholder-'+this.chapter.id;
    if(this.isOverlay) {
      this.backgroundTextStyle = this.sanitizer.bypassSecurityTrustStyle('url(#mask-title-' + this.chapter.id + ')');
      this.maskId = 'overlay-'+ this.chapter.id;
    } else {
      this.backgroundTextStyle = this.chapter.colour;
      this.maskId = this.chapter.id;
      if(this.chapter.subtitle) {
        let sub:Array<string> = this.splitInTwo(this.chapter.subtitle);
        this.subtitle_line1 = sub[0];
        this.subtitle_line2 = sub[1];
      }
    }
  }

  private splitInTwo(data:string):Array<string> {
    let res:Array<string> = [];
    if(data.length > 30) {
      res = data.split(/([\s]+)/);
      let half = Math.floor(res.length/2);
      if(res[half] !== ' ') half = half + 1;
      res[half] = '*';
      res = res.join('').split(/\*/);
    } else {
      res[0] = data;
      res[1] = '';
    }
    return res;
  }
}
