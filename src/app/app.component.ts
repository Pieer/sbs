import { Component } from '@angular/core';
import { ChapterListService } from './chapter-list/chapter-list.service'
import {forEach} from "@angular/router/src/utils/collection";

@Component({
  selector: 'sbs-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [ChapterListService]
})
export class AppComponent {
  isDataAvailable:boolean = false;
  errorMessage: string;
  chapters: any[] = [];
  chaptersDetailList: any[] = [];
  // activeOverlay: boolean = false;
  // showMenu: boolean = false;
  chapterId: number;
  storyId: number;
  // currentChapter: any;

  constructor(chapterListService: ChapterListService) {
    var tag = document.createElement('script');

    tag.src = 'https://www.youtube.com/iframe_api';
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    chapterListService.get()
        .subscribe(
            chapters => {
              this.chapters = chapters;
              this.chaptersDetailList = chapters;
              this.isDataAvailable = true;

              window.setTimeout(function(){
                let html = document.getElementsByTagName('html')[0];
                html.classList.add('loaded');
              }, 0);
            },
            error =>  this.errorMessage = <any>error
        );

    if(navigator.userAgent.match(/Edge/) || navigator.userAgent.match(/Trident/)) { // if IE Edge or 11

      window.setTimeout(function () {
        var html = document.getElementsByTagName('html')[0];
        html.style.overflow = "hidden";
      }, 500);
    }


      // document.querySelector('body').addEventListener("mousewheel", function (event) {
      //     // remove default behavior
      //     event.preventDefault();
      //
      //     //scroll without smoothing
      //     var wheelDelta = event.wheelDelta;
      //     var currentScrollPosition = window.pageYOffset;
      //     window.scrollTo(0, currentScrollPosition - wheelDelta);
      //
      // });

  }
}
