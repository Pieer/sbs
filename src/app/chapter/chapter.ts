export class Chapter {
    id: number;
    active: boolean;
    direction: any;
    mask: any;
    background: string;
    navTitle: string;
    title: string;
    subtitle: string;
    video: string;
    colour: string;
    stories: any;
}
