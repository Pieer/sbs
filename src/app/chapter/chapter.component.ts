import { Component, Input, HostBinding, OnInit} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Chapter } from './chapter';
import { ScrollService } from '../scroll.service';

import { YoutubeService } from '../video-player/video-player.service';
import { VolumeService } from '../volume/volume.service';
import { MediaService } from '../media-controller/media-controller.service';

const debugTransition = false;
const debugChapter = false;
const debugFading = false;

/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
  selector: 'sbs-chapter',
  templateUrl: 'chapter.component.html',
  styleUrls: [ 'chapter.component.scss'],
  viewProviders: [ YoutubeService , MediaService ],
})
export class ChapterComponent implements OnInit {
  @Input() public chapter:Chapter;
  @Input() public chapterIndex:number;
  @Input() public currentChapterId:Chapter;

  @HostBinding('class.active') active:boolean;
  @HostBinding('class.show-video') showVideo:boolean;

  public backgroundTextStyle: any;
  private activeContent:boolean;
  private isMobile:boolean;
  private videoTimeout: any;

  constructor(private scrollService: ScrollService,
              private sanitizer: DomSanitizer,
              private youtubeService: YoutubeService,
              private volumeService: VolumeService
  ) {
    // detect if mobile browser. regex -> http://detectmobilebrowsers.com
    /* tslint:disable:semicolon */
    /* tslint:disable:max-line-length */
    this.isMobile = (function(a:any){return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))})(navigator.userAgent||navigator.vendor);
    /* tslint:enable:max-line-length */
    /* tslint:enable:semicolon */

    if(!this.isMobile) {
      scrollService.transitionEnd.subscribe(() => {
        if (scrollService.goingDown) {
          if (scrollService.isOverlay) {
            if (debugTransition) console.log('01');
            this.addMask();
          } else {
            if (debugTransition) console.log('02');
            this.nextChapter();
          }
        } else {
          if (scrollService.isOverlay) {
            if (debugTransition) console.log('03');
            this.previousChapter();
          }
        }
      });

      scrollService.transitionMaskEnd.subscribe((finalId) => {
        let fi = parseInt(String(finalId).replace(/[^\d.]/g, ''));
        if (scrollService.goingDown) {
          if (fi === this.chapter.id && this.isCurrent()) {
            if (scrollService.isOverlay) {
              this.activeContent = true;
              if (debugTransition) console.log('1');
            } else {
              if (debugFading) console.log('Show video ' + this.chapter.id);
              this.showVideo = true;
              this.videoTimeout = setTimeout(() => {
                this.youtubeService.play();
              }, 500);
              if (debugTransition) console.log('2');
            }
          }
        } else {
          if (scrollService.isOverlay) {
            if (this.isCurrent()) {
              this.active = true;
              this.activeContent = true;
              if (debugTransition) console.log('3');
              // this.youtubeService.play();
            }
            if (this.isPrevious()) {
              this.active = false;
              if (debugTransition) console.log('4');
            }
          } else {
            if (this.isCurrent()) {
              if (debugTransition) console.log('5');
              this.youtubeService.play();
              if (debugFading) console.log('Show video ' + this.chapter.id);
              this.showVideo = true;
            }
          }
        }
      });
    }
  }

  ngOnInit() {
    this.backgroundTextStyle = this.sanitizer.bypassSecurityTrustStyle('url(' + this.chapter.background + ')');
    this.youtubeService.launchPlayer(this.chapter.video, 'video-placeholder-'+this.chapter.id, false);
  }

  isCurrent():boolean {
    if(debugChapter) console.log('isCurrent', this.scrollService.currentChapter === this.chapter.id,
        this.scrollService.currentChapter, this.chapter.id);
    return this.scrollService.currentChapter === this.chapter.id;
  }

  isPrevious():boolean {
    if(debugChapter) console.log('isPrevious', this.scrollService.previousChapter === this.chapter.id,
        this.scrollService.previousChapter , this.chapter.id);
    return this.scrollService.previousChapter === this.chapter.id;
  }

  // Going down
  addMask() {
    if(this.isCurrent()) {
      if(debugFading) console.log('Fade video '+this.chapter.id);
      this.showVideo = false;
      this.youtubeService.pause();
      clearTimeout(this.videoTimeout);
      this.activeContent = false;
      this.active = true;
    }
  }

  nextChapter() {
    if(this.isPrevious()) {
      this.activeContent = true;
      this.active = false;
    }
    if(this.isCurrent()) {
      this.activeContent = false;
      this.active = true;
    } // Replace by addMask?
  }

  // Going up
  previousChapter() {
    if(this.isCurrent()) {
      this.active = false;
      this.activeContent = true;
    }
    if(this.isPrevious()) {
      if(debugFading) console.log('Fade video '+this.chapter.id);
      this.showVideo = false;
      this.activeContent = false; // Necessary?
      this.active = false;
      this.youtubeService.pause();
    }
  }

  convertHex(hex:string,opacity:number) {
    hex = hex.replace('#','');
    let r = parseInt(hex.substring(0,2), 16),
        g = parseInt(hex.substring(2,4), 16),
        b = parseInt(hex.substring(4,6), 16);
    return 'rgba('+r+','+g+','+b+','+opacity/100+')';
  }

}
