import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
// import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SvgMaskComponent } from './svg-mask/svg-mask.component';
import { VideoPlayerComponent } from './video-player/video-player.component';
import { MediaControllerComponent } from './media-controller/media-controller.component';
import { OverlayComponent } from './overlay/overlay.component';
import { SvgOverlayComponent } from './svg-mask/svg-overlay.component'
import { WaveMaskDirective } from './svg-mask/wave-mask.directive';
import { AudioPlayerComponent } from './audio-player/audio-player.component';
import { MenuComponent } from './menu/menu.component';
import { NavbarComponent } from './navbar/navbar.component';
import { VolumeComponent } from './volume/volume.component';
import { ChapterComponent } from './chapter/chapter.component';
import { ChapterListComponent } from './chapter-list/chapter-list.component';
import { StoryComponent } from './story/story.component';
import { StoryDetailComponent } from './story-detail/story-detail.component';
import { StoryListComponent } from './story-list/story-list.component';

import { OverlayService } from './overlay/overlay.service'
import { VolumeService } from './volume/volume.service';
import { ChapterListService } from './chapter-list/chapter-list.service'
import { ScrollService } from './scroll.service';
import { YoutubeService } from './video-player/video-player.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    SvgMaskComponent,
    VideoPlayerComponent,
    MediaControllerComponent,
    OverlayComponent,
    SvgOverlayComponent,
    WaveMaskDirective,
    AudioPlayerComponent,
    MenuComponent,
    NavbarComponent,
    VolumeComponent,
    ChapterComponent,
    ChapterListComponent,
    StoryComponent,
    StoryDetailComponent,
    StoryListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
    // RouterModule
  ],
  providers: [
    OverlayService,
    VolumeService,
    ChapterListService,
    ScrollService,
    YoutubeService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
