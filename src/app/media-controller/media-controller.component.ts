import { Component , HostBinding } from '@angular/core';
import { MediaService } from './media-controller.service';
import { OverlayService } from '../overlay/overlay.service';

/**
 * This class represents the lazy loaded AudioComponent.
 */
@Component({
  selector: 'sbs-media-controller',
  templateUrl: 'media-controller.component.html',
  styleUrls: [ 'media-controller.component.scss' ]
})
export class MediaControllerComponent {
  @HostBinding('class.playing') isPlaying:boolean;
  @HostBinding('class.fullscreen') isFullScreen:boolean;

  constructor(private overlayService: OverlayService, private mediaService: MediaService) {}

  playPause(e:any): void {
    this.isPlaying = !this.isPlaying;
    if(this.isPlaying) {
      this.pause();
    } else {
      this.play();
    }
  }

  play() {
    this.mediaService.controllerPlay();
  }

  pause() {
    this.mediaService.controllerPause();
  }

  restart() {
    this.mediaService.controllerRestart();
  }

  toggleFullScreen() {
    this.isFullScreen = !this.isFullScreen;
    this.overlayService.goFullScreen();
  }
}
