/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MediaControllerService } from './media-controller.service';

describe('MediaControllerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MediaControllerService]
    });
  });

  it('should ...', inject([MediaControllerService], (service: MediaControllerService) => {
    expect(service).toBeTruthy();
  }));
});
