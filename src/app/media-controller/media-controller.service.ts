import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';

@Injectable()
export class MediaService {

  // Observable string sources
  playSource = new Subject<any>();
  pauseSource = new Subject<any>();
  restartSource = new Subject<any>();

  // Observable string streams
  play = this.playSource.asObservable();
  pause = this.pauseSource.asObservable();
  restart = this.restartSource.asObservable();

  controllerPlay() {
    this.playSource.next('');
  }

  controllerPause() {
    this.pauseSource.next('');
  }

  controllerRestart() {
    this.restartSource.next('');
  }
}
