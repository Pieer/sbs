import { Component, Input, AfterViewInit, OnDestroy, HostListener } from '@angular/core';
import { Chapter } from '../chapter/chapter';
import { ScrollService } from '../scroll.service';
import { OverlayService } from '../overlay/overlay.service';

const UP = 38;
const DOWN = 40;
const LEFT = 37;
const RIGHT = 39;
const ESCAPE = 27;

/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
  selector: 'sbs-chapter-list',
  templateUrl: 'chapter-list.component.html',
  styleUrls: [ 'chapter-list.component.scss' ]
})
export class ChapterListComponent implements AfterViewInit, OnDestroy {
  @Input() chapters:Array<Chapter> = [];
  @Input() public noTransition:boolean;

  public nav:Array<any> = [];
  public currentChapter:Chapter;
  public currentChapterId:number;


  destroyed:boolean = false;

  constructor(private scrollService: ScrollService,
              private overlayService: OverlayService
  ) {}

  /**
   * Trigger change on the child to update mask animation
   */
  ngAfterViewInit():void {
    let len = this.chapters.length;
    for (let i = 0; i <= len; ++i) {
      if(this.chapters[i] !== undefined) {
        this.nav.push({
          title: this.chapters[i].title,
          navTitle:  (i === 0) ? 'Introduction' : this.chapters[i].title,
          id:  this.chapters[i].id,
          colour:  this.chapters[i].colour
        });
      }

      this.scrollService.addScene(document.querySelector('#chapter-content-' + i), this.updateMask.bind(this), '-100%');
      this.scrollService.addScene(document.querySelector('#chapter-header-' + i), this.scrollChapter.bind(this) , '0');
      if(this.chapters[i] && this.chapters[i].stories.length) {
        let stl =  this.chapters[i].stories.length;

        for (let j = 0; j <= stl; ++j) {
          // Add fading effect
          this.scrollService.addPanel('#story-'+(i+1)+'-'+j);
        }
      }
    }
    // console.log(thie.nav)
    // Add one last scroll event for footer
    this.scrollService.addScene(document.querySelector('#chapter-header-'+(len+1)), this.scrollChapter.bind(this) , '0');
  }

  /**
   * Trigger change on the child to change chapter
   *
   * @param {e} Event - ScrollMagic event.
   */
  scrollChapter(e:any):void {
    let goingDown = e['scrollDirection'] !== 'REVERSE';
    this.scrollService.goNext(e['scrollDirection'], goingDown ? 'chapter':'overlay');

    this.currentChapterId = this.scrollService.currentChapter;
  }

  /**
   * Trigger change on the child to update mask animation
   *
   * @param {e} Event - ScrollMagic event.
   */
  updateMask(e:any):void {
    let goingDown = e['scrollDirection'] !== 'REVERSE';
    this.scrollService.goNext(e['scrollDirection'], goingDown ? 'overlay':'chapter');
  }

  /**
   * Remove componet from memory
   */
  ngOnDestroy():void {
    this.destroyed = true;
  }

  /**
   * Select Chapter, check direction, check if transition is in progress
   *
   * @param {ChapterComponent} nextChapter - The next chapter to select
   * @param {Direction} direction - The direction
   */
  select(nextChapter:number, direction:string):void {
    // Prevent this user-triggered transition from occurring if there is
    // already one in progress
    if(this.currentChapter) {
      if (nextChapter !== this.currentChapter.id) {
        this.goNext(this.chapters[nextChapter], direction);
      }
    } else {
      this.goNext(this.chapters[nextChapter], direction);
    }
  }

  /**
   * Trigger next chapter
   */
  next():any {
    let newIndex = this.getCurrentIndex() + 1;
    if(newIndex > this.chapters.length) newIndex = this.chapters.length-1;
    return this.select(newIndex, 'FORWARD');
  }

  /**
   * Trigger previous chapter
   */
  prev():any {
    let newIndex = this.getCurrentIndex() - 1 < 0 ? 0 : this.getCurrentIndex() - 1;
    return this.select(newIndex, 'REVERSE');
  }

  /**
   * Trigger next chapter
   *
   * @param {ChapterComponent} chapter - The next chapter is trigger by the change of the binded direction property
   * @param {Direction} direction - The direction
   */
  goNext(chapter:Chapter, direction:string):void {
    if (this.destroyed) {
      return;
    }

    chapter.direction = direction;

    if (this.currentChapter) {
      this.currentChapter.direction = direction;
    }

    this.currentChapter = chapter;
    this.currentChapterId = chapter.id;
  }

  /**
   * Get current index and avoid initiate the first panel
   */
  getCurrentIndex():number {
    return !this.currentChapter ? -1 : this.currentChapter.id;
  }

  @HostListener('document:keyup', ['$event'])
  onKeyUp(event:KeyboardEvent) {
    let keynum = event.keyCode;
    // console.log(keynum)
    if(this.overlayService.isFrozen) {
      if(keynum === UP || keynum === LEFT) {
        this.overlayService.prev();
      }
      if(keynum === DOWN || keynum === RIGHT) {
        this.overlayService.next();
      }
      if(keynum === ESCAPE) {
        this.overlayService.closeOverlay();
      }
      // } else {
      //     if(keynum === UP || keynum === LEFT) {
      //         if(this.currentChapterId>=1) {
      //             this.currentChapterId = this.currentChapterId - 1;
      //         }
      //     }
      //     if(keynum === DOWN || keynum === RIGHT) {
      //         if(this.currentChapterId < this.chapters.length -1) {
      //             this.currentChapterId = this.currentChapterId + 1;
      //         }
      //     }
      //     window.location.href = '#'+this.chapters[this.currentChapterId].title;
    }
    // this.scrollService.skip(this.currentChapterId);
  }
}
