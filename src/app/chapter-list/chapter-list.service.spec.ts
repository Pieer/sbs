/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ChapterListService } from './chapter-list.service';

describe('ChapterListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChapterListService]
    });
  });

  it('should ...', inject([ChapterListService], (service: ChapterListService) => {
    expect(service).toBeTruthy();
  }));
});
