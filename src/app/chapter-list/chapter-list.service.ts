import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

/**
 * This class provides the ChapterList service with methods to read chapters.
 */
@Injectable()
export class ChapterListService {

  /**
   * Creates a new ChapterListService with the injected Http.
   * @param {Http} http - The injected Http.
   * @constructor
   */
  constructor(private http: Http) {}

  /**
   * Returns an Observable for the HTTP GET request for the JSON resource.
   * @return {string[]} The Observable for the HTTP request.
   */
  get(): Observable<string[]> {
    //noinspection TypeScriptUnresolvedFunction
    return this.http.get('/assets/data_contentful.json')
        .map((res: Response) => {
          //return this.sortChapters(res.json());
          return res.json();
        })
        .catch(this.handleError);
  }

  /**
   * Handle HTTP error
   */
  private handleError (error: any) {
    // We could dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
        error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }


  /**
   * Sort chapter into column and add style
   */
  //sortChapters(chapters:any) {
  //  //let half = Math.ceil(chapters.length / 2);
  //  //let leftSide = chapters.splice(0, half);
  //  //let rightSide = this.chapters.splice(half, this.chapters.length - half);
  //  return chapters;
  //}
}

