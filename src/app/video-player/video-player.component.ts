import { Component , Input } from '@angular/core';
import { YoutubeService } from '../video-player/video-player.service';
import { OverlayService } from '../overlay/overlay.service';

import { MediaService } from '../media-controller/media-controller.service';
import 'rxjs/add/operator/map';

/**
 * This class represents the lazy loaded OverlayComponent.
 */
@Component({

  selector: 'sbs-video-player',
  templateUrl: 'video-player.component.html',
  styleUrls: [ 'video-player.component.scss' ]
})
export class VideoPlayerComponent {

  @Input() videoId: string;
  @Input() placeholder: string;
  overlayService: any;
  isPlaying: boolean = true;

  constructor(overlayService: OverlayService,
              private youtubeService: YoutubeService,
              private mediaService: MediaService) {

    this.overlayService = overlayService;

    mediaService.play.subscribe( () => {
      this.play();
    });

    mediaService.pause.subscribe( () => {
      this.pause();
    });

    mediaService.restart.subscribe( () => {
      this.restart();
    });
  }

  play(): void {
    this.isPlaying = true;
    this.youtubeService.youtube.player.playVideo();
  }

  pause(): void {
    this.isPlaying = false;
    this.youtubeService.youtube.player.pauseVideo();
  }

  restart() {
    this.youtubeService.youtube.player.seekTo(0);
  }
}
