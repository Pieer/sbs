import { Injectable, HostListener, NgZone } from '@angular/core';
import { window } from '@angular/platform-browser/src/facade/browser';
import { VolumeService } from '../volume/volume.service';

@Injectable()
export class YoutubeService {
  @HostListener('window:resize', ['$event'])

  video: any;
  fluidRatio: number = 1.78;
  autoPlay: boolean;
  playerTimeout: any;

  maxTry: number = 5;

  youtube: any = {
    ready: false,
    player: null,
    playerId: null,
    videoId: null,
    videoTitle: null,
    playerHeight: '100%',
    playerWidth: '100%'
  };

  constructor (private volumeService: VolumeService, private ngZone:NgZone) {
    this.setupPlayer();
    this.autoPlay = false;

    this.subscribe();
    if(typeof window.onresize === 'object') {
      window.onresize = () => {
        ngZone.run(() => {
          this.resizeAll();
        });
      };
    }

  }

  subscribe():void {
    this.volumeService.play.subscribe( () => {
      this.unMute();
    });

    this.volumeService.pause.subscribe( () => {
      this.mute();
    });
  }

  unMute():void {
    if(this.youtube.player && this.youtube.player.unMute !== 'undefined') {
      this.youtube.player.unMute();
    }
  }

  mute():void {
    if(this.youtube.player && this.youtube.player.mute !== 'undefined') {
      this.youtube.player.mute();
    }
  }

  fluidVid () {

    // Find all YouTube videos
    this.video = document.querySelector('#'+this.youtube.playerId);

    if(this.video) {
      this.onResize(this.video);
    }

  }

  onResize(video:any) {
    // Resize video according to their own aspect ratio
    if(window.innerHeight*this.fluidRatio > window.innerWidth) {
      let newWidth = window.innerHeight*this.fluidRatio;
      video.setAttribute('style','width:'+newWidth+'px; left: 50%; margin-left:-'+(newWidth/2)+'px;height:'+window.innerHeight+'px;');
    } else {
      let newHeight = window.innerWidth/this.fluidRatio;
      let marginTop = newHeight - window.innerHeight/2;
      video.setAttribute('style','width:'+window.innerWidth+'px; top: 50%; margin-top:-'+marginTop+'px;height:'+newHeight+'px;');
    }
  }

  resizeAll() {
    let videos = document.querySelectorAll('iframe');
    [].forEach.call(videos, (el:any) =>
        this.onResize(el)
    );
  }

  bindPlayer(elementId:string): void {
    this.youtube.playerId = elementId;
  };

  createPlayer(id:string): void {
    // Remove this
    // return false;
    return new window.YT.Player(this.youtube.playerId, {
      height: this.youtube.playerHeight,
      width: this.youtube.playerWidth,
      videoId: id,
      playerVars: {
        'showinfo':0,
        'iv_load_policy':3,
        'modestbranding':1,
        'nologo':1,
        'rel': 0,
        'controls': 0,
        'enablejsapi': 1,
        'autoPlay': this.autoPlay,
        'disablekb': 1,
        'loop':1,
        'wmode': 'transparent',
        'origin': location.href
      },
      events: {
        'onReady': () => {
          // if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
          //   if (this.autoPlay) {
          //     this.play();
          //   }
          // }
          if(!this.volumeService.isPlaying()) {
            if(this.youtube.player !== null && this.youtube.player !== undefined) {
              this.youtube.player.mute();
            }
          }
        }
      }
    });
  }

  loadPlayer(id:string): void {
    if (this.youtube.playerId) {
      if (this.youtube.player) {
        this.youtube.player.destroy();
      }
      this.youtube.player = this.createPlayer(id);
      // console.log('create player', this.youtube.player);
      this.fluidVid();
    }
  }

  //removePlayer () {
  //    this.youtube.player.destroy();
  //}

  setupPlayer () {
    // in production mode, the youtube iframe api script tag is loaded
    // before the bundle.js, so the 'onYouTubeIfarmeAPIReady' has
    // already been triggered
    // TODO: handle this in build or in nicer in code
    // console.log ('Running Setup Player', this);
    window['onYouTubeIframeAPIReady'] = () => {
      if (window['YT']) {
        // console.log('Youtube API is ready');
        this.youtube.ready = true;
      }
    };

    if (window.YT && window.YT.Player) {
      // console.log('Youtube API is ready');
      this.youtube.ready = true;
    }
  }

  pause(): void {
    // console.log('pause')
    clearTimeout(this.playerTimeout);
    if( this.youtube.player !== null ) {
      if( this.youtube.player.pauseVideo !== undefined ) {
        this.youtube.player.pauseVideo();
      }
    }
  }

  play(): void {
    let isReady = false;
    if( this.youtube.player !== null) {
      isReady = this.youtube.player.playVideo !== undefined;
    }
    if(isReady) {
      this.youtube.player.playVideo();
    } else {
      if(this.maxTry-- > 0) {
        this.playerTimeout = setTimeout( () => {
          this.play();
        },1000);
      }
    }
  }

  launchPlayer(id:string, placeholder:string, autoPlay:boolean):void {
    this.autoPlay = autoPlay;
    this.bindPlayer(placeholder);
    // Source of pb ?
    setTimeout( () => {
      this.loadPlayer(id);
    }, 2000);
    return this.youtube;
  }
}
