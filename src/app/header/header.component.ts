import { Component } from '@angular/core';
import { OverlayService } from '../overlay/overlay.service';

/**
 * This class represents the header component.
 */
@Component({
  selector: 'sbs-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  showIconMenu: boolean = false;

  constructor(private overlayService: OverlayService) {
    overlayService.close.subscribe( () => {
      this.showIconMenu = false;
    });
  }

  openNav() {
    this.showIconMenu = true;
    this.overlayService.openNav();
    this.overlayService.toggleScroll(true);
  }
}

