/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { VolumeService } from './volume.service';

describe('VolumeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VolumeService]
    });
  });

  it('should ...', inject([VolumeService], (service: VolumeService) => {
    expect(service).toBeTruthy();
  }));
});
