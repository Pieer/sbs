import { Component, HostBinding } from '@angular/core';
import { VolumeService } from '../volume/volume.service';

/**
 * This class represents the lazy loaded OverlayComponent.
 */
@Component({
  selector: 'sbs-volume',
  templateUrl: 'volume.component.html',
  styleUrls: [ 'volume.component.scss' ]
})
export class VolumeComponent {
  @HostBinding('class.playing') isPlaying:boolean;

  constructor(private volumeService: VolumeService) {
    this.isPlaying = volumeService.isPlaying();

    volumeService.play.subscribe( () => {
      this.isPlaying = true;
    });

    volumeService.pause.subscribe( () => {
      this.isPlaying = false;
    });
  }

  playPause(e:any): void {
    if(this.isPlaying) {
      this.pause();
    } else {
      this.play();
    }
  }

  play() {
    this.volumeService.controllerPlay();
  }

  pause() {
    this.volumeService.controllerPause();
  }
}
