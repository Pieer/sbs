import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';

@Injectable()
export class VolumeService {
  sound: boolean = true;

  // Observable string sources
  playSource = new Subject<any>();
  pauseSource = new Subject<any>();

  // Observable string streams
  play = this.playSource.asObservable();
  pause = this.pauseSource.asObservable();


  public isPlaying():boolean {
    return this.sound;
  }

  controllerPlay() {
    this.sound = true;
    this.playSource.next('');
  }

  controllerPause() {
    this.sound = false;
    this.pauseSource.next('');
  }
}
