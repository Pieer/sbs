# -*- coding: utf-8 -*-

from contentful import Client
from contentful.cda.fields import *
from contentful.cda.resources import *
from pprint import pprint
import json

class Story(Entry):
    __content_type__ = 'story'

    title = Field(Symbol)
    shortDescription = Field(Text)
    fullDescription = Field(Text)
    video = Field(Text)
    image = Field(Link)
    thumbnail = Field(Link)
    year = Field(Symbol)
    chapters = Field(Array)

class Chapter(Entry):
    __content_type__ = 'chapter'

    title = Field(Symbol)
    subtitle = Field(Text)
    video = Field(Text)
    background = Field(Link)
    thumbnail = Field(Link)
    stories = Field(Array)
    order = Field(Number)

root = []

client = Client('9rwuwxdbojns', '85ba50487c7790efa213813a9d70cdb65cf7034430741a0db9b1355b5f9f54fe',custom_entries=[Story, Chapter])
chapters = client.fetch(Chapter).all()
chapter_counter = 0

chapters = sorted(chapters, key=lambda chapter: chapter.fields['order'])


for chapter in chapters:
    chapter_counter = chapter_counter+1
    chap = {
        "id": chapter_counter,
        "background": ""+chapter.fields['background'].url+"",
        "thumbnail": ""+chapter.fields['thumbnail'].url+"",
        "title":  ""+chapter.title+"",
        "subtitle":  ""+chapter.fields['subtitle']+"",
        "colour": ""+chapter.fields['colour']+"",
        "video": ""+chapter.fields['video']+"",
        "stories": [],
    }

    root.append(chap)

    if chapter.stories:
        stor_counter = 0
        for story in chapter.stories:
            stor_counter = stor_counter + 1

            if "category" in story.fields:
              category = story.fields['category'][0]
            else:
              category = ""

            if "year" in story.fields:
              year = story.fields['year']
            else:
              year = ""

            stor = {
                "id" : stor_counter,
                "title": "" + getattr(story, "title", "") + "",
                "description": "" + getattr(story, "shortDescription", "") + "",
                "content": "" + getattr(story, "fullDescription", "") + "",
                "category": "" + category + "",
                "year": "" + year + "",
                "url": "",
                "type": "text"
            }

            if "image" in story.fields:
                stor["url"] =  "" + story.image.url  + ""
                stor["type"] = "image"

            if "video_id" in story.fields:
                stor["video"] =   story.fields['video_id']
                stor["type"] = "video"

            if "audio" in story.fields:
                stor["source"] =  "" + story.fields['audio'].url  + ""
                stor["type"] = "audio"



            chap["stories"].append(stor)

with open('../src/assets/data_contentful.json', 'w') as outfile:
    json.dump(root, outfile)

