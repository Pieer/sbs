# Contentful scraper

Installation

```sh
$ mkvirtualenv sbs
$ cd contentful
$ pip install -r requirements.txt
$ python scrape.py
```
