import { SBSAppPage } from './app.po';

describe('sbs-app App', function() {
  let page: SBSAppPage;

  beforeEach(() => {
    page = new SBSAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
